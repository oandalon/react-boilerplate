# React Boilerplate

## Build Instructions

### Requirements

* Node v6.9.1

### Install Modules

```bash
$ npm install
```

### Build For Production

```bash
$ npm build
```

This will output all the files needed into the `/dist` directory.

##### Output files

* `bundle.<version>.<hash>.js`: Contains all the Sitemap specific code
* `vendor.<hash>.js`: Contains all the 3rd party modules (i.e. react, lodash, etc.)
* `index.html`: Auto generated links to all the js files created during the build process

### Run Local Dev Server

This will run the webpack dev server which will create an adhoc build whenever anything changes in the src directory.

```bash
$ npm start
```

### Testing

Test results are displayed in the console.

```bash
$ npm run test
```

### Linting

Test results are displayed in the console.

```bash
$ npm run lint
```

### Coverage

Generate code coverage report:

```bash
$ npm run istanbul
```

Now you can open the generated HTML file located at _./coverage/Icov-report/index.html_, on the root of your repo.