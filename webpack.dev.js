const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const configLoader = require('./config/configLoader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const _ = require('lodash');
const colors = require('colors');
const prettyjson = require('prettyjson');

const configuration = configLoader.getConfig();
const configEnv = _.get(process, 'env.CONFIG_ENV', 'local');
const configToUse = _.get(configuration, configEnv, {});
const context = path.resolve(__dirname, 'src');

// Show current settings
console.log(`\nEnvironment: ${colors.blue(process.env.NODE_ENV)}`);
console.log(
  `Build Output Directory: ${colors.blue(path.resolve(__dirname, 'dist'))}`
);
console.log(`Output: ${colors.blue('[name].js')}`);
console.log(colors.green('Configuration:'));
console.log(
  _.isEmpty(configuration) ? 'EMPTY' : prettyjson.render(configuration)
);
console.log('');

module.exports = merge(common, {
  entry: {
    'dev.a': 'react-hot-loader/patch',
    'dev.b': 'webpack-dev-server/client?http://localhost:8080',
    'dev.c': 'webpack/hot/only-dev-server'
  },

  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: './index.local.html',
      filename: './index.html',
      configJson: JSON.stringify(configToUse)
    }),
    new ExtractTextPlugin({
      filename: '[name].css',
      disable: false,
      allChunks: true
    })
  ],

  resolve: { symlinks: false },

  output: {
    chunkFilename: '[name].js',
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },

  devtool: 'inline-source-map',

  devServer: {
    hot: true,
    contentBase: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    historyApiFallback: true,
    disableHostCheck: true
  },

  module: {
    rules: [
      // Styles (css and sass) in src and component libs
      {
        test: /\.(css|scss)$/,
        exclude: [path.join(context, 'styles', 'main.scss')],
        include: [
          /node_modules(\/|\\)@cbtnuggets\/lib-react-component-.+(\/|\\)(lib)(\/|\\)/,
          context
        ],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                importLoaders: 1
              }
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: true }
            }
          ]
        })
      }
    ]
  }
});
