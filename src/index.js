import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import rootReducer from './reducers';
import PageComponent from './components/PageComponent';
import './style/main.scss';

/* global ENV_CONFIGURATION */

const history = createHistory();
const configuration = ENV_CONFIGURATION;

const initialState = {
  configuration,
};

const middleware = applyMiddleware(routerMiddleware(history), thunkMiddleware);

const enhancer = compose(middleware);

const store = createStore(
  rootReducer,
  initialState,
  process.env.NODE_ENV === 'production'
    ? enhancer
    : composeWithDevTools({ name: 'boilerplate' })(enhancer),
);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <PageComponent />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
