import { getServiceSiteGatewayUrl, axiosRequest } from './helper';
import {
  FETCH_POSTS_PENDING,
  FETCH_POSTS_REJECTED,
  FETCH_POSTS_RESOLVED,
} from '../actions/types';

export function postsPending() {
  return {
    type: FETCH_POSTS_PENDING,
  };
}

export function postsError(error) {
  return {
    type: FETCH_POSTS_REJECTED,
    error,
  };
}

export function postsSuccess(posts) {
  return {
    type: FETCH_POSTS_RESOLVED,
    payload: posts,
  };
}

export function fetchPosts(data = {}) {
  return (dispatch, getState) => {
    const serviceUrl = getServiceSiteGatewayUrl(getState, 'getAllPosts');

    dispatch(postsPending());

    return axiosRequest({
      url: `${serviceUrl}`,
      method: 'get',
      params: data,
    })
      .then(result => dispatch(postsSuccess(result.data)))
      .catch(result => dispatch(postsError(result.data)));
  };
}
