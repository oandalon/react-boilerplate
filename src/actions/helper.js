import _ from 'lodash';
import axios from 'axios';

/* istanbul ignore next */
function getServiceSiteGatewayUrl(getState, endpoint) {
  const currentState = getState();
  const siteGatewayService = _.get(
    currentState,
    'configuration.services.service-site-gateway',
    '',
  );
  const { endpoints, baseUrl } = siteGatewayService;
  return `${baseUrl}/${endpoints[endpoint]}`;
}

/* istanbul ignore next */
function axiosRequest(request) {
  return axios(request);
}

export { getServiceSiteGatewayUrl, axiosRequest };
