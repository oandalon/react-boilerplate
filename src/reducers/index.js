import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import { postsReducer } from './postsReducer';

const rootReducer = combineReducers({
  posts: postsReducer,
  form: formReducer,
  routing: routerReducer,
  configuration: (state = {}) => state,
});

export default rootReducer;
