import {
  FETCH_POSTS_PENDING,
  FETCH_POSTS_REJECTED,
  FETCH_POSTS_RESOLVED,
} from '../actions/types';

const INITIAL_STATE = { posts: [], error: null, loading: false };

export function postsReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_POSTS_PENDING:
      return {
        ...state,
        posts: [],
        error: null,
        loading: true,
      };
    case FETCH_POSTS_REJECTED:
      return {
        ...state,
        posts: [],
        error: action.error,
        loading: false,
      };
    case FETCH_POSTS_RESOLVED:
      return {
        ...state,
        posts: action.payload.map(post => ({
          id: post.id,
          title: post.title,
          body: post.body,
        })),
        error: null,
        loading: false,
      };
    default:
      return state;
  }
}
