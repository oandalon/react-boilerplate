import React from 'react';
import { Route } from 'react-router-dom';
import { it, describe } from 'mocha';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow } from 'enzyme';
import 'jsdom-global/register';
import PageComponent from '.';

chai.use(chaiEnzyme());

describe('RoutingComponent', () => {
  it('should render a Route elements', () => {
    const PageComponentWrapper = shallow(<PageComponent />);
    expect(PageComponentWrapper.find(Route).length).to.equal(2);
  });
});
