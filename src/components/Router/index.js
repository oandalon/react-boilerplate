import React from 'react';
import { Route } from 'react-router-dom';
import { HomeContainer } from '../../containers';

export default function Router() {
  return (
    <div>
      <Route exact path="/" component={HomeContainer} />
    </div>
  );
}
