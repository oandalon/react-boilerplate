import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import Router from '../Router';

export function PageComponent() {
  return (
    <div id="site-content">
      <div className="navbar">Navbar</div>
      <Router />
      <div className="footer">Footer</div>
    </div>
  );
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    host: _.get(state, 'configuration.host', ''),
    env: _.get(state, 'configuration.env', 'local'),
  };
}

export default withRouter(connect(mapStateToProps)(PageComponent));
