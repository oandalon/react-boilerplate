import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

export default function HomeComponent({ posts }) {
  const postsList =
    !posts.loading && posts.posts ? (
      posts.posts.map(post => <li key={post.id}>{post.title}</li>)
    ) : (
      <ul>
        <li>
          <span>Loading...</span>
        </li>
      </ul>
    );

  return (
    <div className="Home-container">
      <h1>Home</h1>
      <h2>Posts</h2>
      <ul>{postsList}</ul>
    </div>
  );
}

HomeComponent.propTypes = {
  posts: PropTypes.object.isRequired,
};
