import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchPosts } from '../actions';
import HomeComponent from '../components/HomeComponent';

export class HomeContainer extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    return <HomeComponent {...this.props} />;
  }
}

const mapStateToProps = state => ({
  posts: state.posts,
});

const mapDispatchToProps = {
  fetchPosts,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);


HomeContainer.propTypes = {
  fetchPosts: PropTypes.func.isRequired,
};
