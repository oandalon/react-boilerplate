const merge = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const configLoader = require('./config/configLoader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const _ = require('lodash');

const configuration = configLoader.getConfig();
const prodConfigToUse = _.get(configuration, 'prod', {});
const qaConfigToUse = _.get(configuration, 'qa', {});

module.exports = merge(common, {
  plugins: [
    new UglifyJSPlugin(),
    new ExtractTextPlugin({
      filename: '[name].[chunkhash].css',
      disable: false,
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: './index.prod.html',
      filename: 'index.prod.html',
      configJson: JSON.stringify(prodConfigToUse)
    }),
    new HtmlWebpackPlugin({
      template: './index.stage.html',
      filename: 'index.stage.html',
      configJson: JSON.stringify(prodConfigToUse)
    }),
    new HtmlWebpackPlugin({
      template: './index.qa.html',
      filename: 'index.qa.html',
      configJson: JSON.stringify(qaConfigToUse)
    })
  ],

  output: {
    chunkFilename: '[name].[chunkhash].js',
    filename: '[name].[chunkhash].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/assets/'
  },

  devtool: false,
  devServer: {
    hot: false,
    contentBase: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    historyApiFallback: true,
    disableHostCheck: true
  },

  module: {
    rules: [
      // Styles (css and sass) in src and component libs
      {
        test: /\.(css|scss)$/,
        include: [
          /node_modules(\/|\\)@cbtnuggets\/lib-react-component-.+(\/|\\)(lib)(\/|\\)/,
          path.resolve(__dirname, 'src')
        ],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: false,
                importLoaders: 1
              }
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: true }
            }
          ]
        })
      }
    ]
  }
});
