/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */
require('babel-polyfill');
const path = require('path');
const webpack = require('webpack');
const WriteFilePlugin = require('write-file-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const packageJson = require('./package.json');

const { version, name } = packageJson;

const context = path.resolve(__dirname, 'src');

module.exports = {
  entry: {
    [`${packageJson.name}.${packageJson.version}`]: [
      'babel-polyfill',
      './index.js'
    ],
    vendor: [
      'react',
      'react-dom',
      'lodash',
      'axios',
      'history',
      'prop-types',
      'react-redux',
      'redux',
      'redux-form',
      'redux-thunk'
    ]
  },

  plugins: [
    new WriteFilePlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      VERSION: JSON.stringify(version),
      SITE_NAME: JSON.stringify(name),
      'typeof window': JSON.stringify('object')
    })
  ],
  context,

  resolve: { symlinks: false },

  module: {
    rules: [
      // Javascript
      {
        test: /\.js$/,
        exclude: [/node_modules/, /dev/],
        include: [
          /node_modules(\/|\\)@cbtnuggets\/lib-react-component-.+(\/|\\)(lib)(\/|\\)/,
          context
        ],
        loader: 'babel-loader'
      },

      // Style (just main.scss)
      {
        test: /\.scss$/,
        include: [path.join(context, 'styles', 'main.scss')],
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader!sass-loader'
        })
      },

      // Images
      {
        test: /\.(png|jpg|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
        use: 'url-loader?limit=100'
      }
    ]
  }
};
